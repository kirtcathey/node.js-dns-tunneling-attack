# Node.js DNS Tunneling Attack

This is a script that leverages DNSlivery to download a stager, which then downloads an obfuscated version of dnscat2.exe to establish a covert DNS tunnel for DNS C2. The very simple stager PowerShell script is also included, which will download the obfuscated dnscat2.exe (and renamed) file via http from the location of your choice.
